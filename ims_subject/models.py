from django.db import models
from ims_base.models import AbstractBaseDetail
from reusable_models import get_model_from_string


class Subject(AbstractBaseDetail):
    subject_code = models.CharField(max_length=255, null=True, blank=True)
    faculties = models.ManyToManyField(get_model_from_string("FACULTY"), blank=True)


class Syllabus(AbstractBaseDetail):
    is_active = models.BooleanField(default=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    year_issued = models.PositiveSmallIntegerField()
