from django.apps import AppConfig


class IMSSubjectConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_subject"

    model_strings = {
        "SUBJECT": "Subject",
    }
